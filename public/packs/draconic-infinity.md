---
Title: 'Draconic Infinity'
Author: 'NafriUkgnet'
Description: 'Minecraft roleplays and Hardcore mode'
Image: 'draconic-infinity.jpeg'
URL: 'https://www.curseforge.com/minecraft/modpacks/draconic-infinity'
Categories: ['exploration','hardcore','magic','tech','multiplayer']
---
Welcome to Draconic Infinity, the multi-purpose kitchensink modpack designed for Minecraft roleplays and Hardcore mode. It also comes with a little bit of questing that'll guide you to the main goal of the pack

Features :

*   Build your own custom weapons, tools, and armours with Tinkers Construct and Construct's Armoury
*   Beautiful World Generation with Biomes O' Plenty

*   Being OP with Draconic Evolution, ProjectE, and Avaritia

*   Exploration with Twilight Forest, The Aether, and Roguelike Dungeons.
*   Very Expensive yet Easy End Game Recipe

*   Build your base like never before with Chisel and ArchitectureCraft

To play this pack, you have to allocate 4.5GB RAM. And for those who want to play with shaders, I recommend using Complementary Shaders by Emin with Compatibility Mode On

Join Our Discord Server : [https://discord.gg/m9NTMV2NHq](https://discord.gg/m9NTMV2NHq)

Are you tired of being lonely and want to play with your friends?

[![](https://www.bisecthosting.com/partners/custom-banners/f4c293a2-9b0d-4670-a27e-e719dc2a6625.png)](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fbisecthosting.com%252fnaffynafnaf)  Click on the picture above, select plan (at least 5GB), use my code **naffynafnaf** to get **25%** off your first month and enjoy playing with your friends!

[![](https://media.discordapp.net/attachments/862012820325269524/862047906310914078/Ring-Packs-Affiliated-Modpacks-Banner_Zeichenflache_1.png)](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fpackring.gitlab.io%252fpackring%252f)
