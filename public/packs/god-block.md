---
Title: 'God Block'
Author: 'Kreezxil'
Description: 'You are God! In the beginning.'
Image: 'god-block.png'
URL: 'https://www.curseforge.com/minecraft/modpacks/god-block'
Categories: ['skyblock','exploration','magic','quests','tech']
---

You appear in a void world where a light haze has just been introduced. By harvesting the nothingness around you quanta is formed. You will then craft the quanta together with itself to form the base forms of matter for your world. From the emerging world you will be able to create higher more advanced forms of matter that will you to do ever more amazing things. Eventually you will gain the power of dimensional travel and the ability to even recreate the god block.

Quest Driven but not required for enjoyment.

## Servers

Set your level-type to: voidworld

[![](https://www.lduhtrp.net/image-100472236-14087903)](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fwww.kqzyfj.com%252fclick-100472236-14087903)

## Support God Block

There are a few ways to support this project. You can use [https://patreon.com/kreezxil](https://patreon.com/kreezxil), or rent a server from Bisect Hosting, or if neither of those appeal to you. Buy some merchandise for yourself, office, friends, relatives, children, enemies.

## Help a Veteran today

I am Veteran of United States Army. I am not legally disabled. But I do love to make these mods and modpacks for you guys. Please help me to help you by Donating at [![](https://i.imgur.com/WhT4CbN.png)](https://patreon.com/kreezxil).

[![packring](https://i.imgur.com/1ah07M7.png)](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fpackring.gitlab.io%252fpackring%252f)
