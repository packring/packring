---
Title: 'Ascension Of Chaos'
Author: 'thenimbleninja'
Description: 'Ascension Of Chaos is an homage to Project Ozone 2 Kappa Mode.'
Image: 'ascension-of-chaos.png'
URL: 'https://www.curseforge.com/minecraft/modpacks/ascension-of-chaos'
Categories: ['skyblock','large','magic','quests','tech']
---

![](https://www.bisecthosting.com/images/CF/Scientific_Dreams/BH_SD_Overview.png)

Ascension Of Chaos is an homage to Project Ozone 2 Kappa Mode. the pack has some items and aspects that pack has as i liked those aspects. In my modpack i tried to make the pack harder than Kappa mode and will hopefully eventually have 2 or 3 game modes. The pack has a few difficulty adding mods lycanites makes living a lot harsher during night it will be pitch black thanks to hardcore darkness. . The pack will have over one thousand quests when finished. It has a reward store where you buy certain items with the cash you get doing quests. You can also eventually sell a ton of items for cash on repeat to buy rewards. The pack will have a ton of custom worlds to make the pack more special. The pack also has custom loot bags and quest box rewards. The pack will also have custom chance cubes when complete. Sadly the only bad thing i can say about my modpack is that it needs 8+ gigs of ram to run as it has around 300+ mods.

when you load a world click on  more world options and on topography click on customize and pick a world from there!

![](https://www.bisecthosting.com/images/CF/Scientific_Dreams/BH_SD_Community.png)

[![](https://i.imgur.com/4k8lKdV.png)](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fwww.guilded.gg%252fi%252fRkrdW7lE)[![](https://i.imgur.com/uabSTGk.png)![](https://cdn.discordapp.com/attachments/862061100933972038/865086703261974598/packring-logo.png)](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fpackring.gitlab.io%252fpackring%252f)

[![](https://www.bisecthosting.com/images/CF/Scientific_Dreams/BH_SD_PromoCard.png)](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fbisecthosting.com%252fTheNimbleNinja)

**BisectHosting** offers quick, simple, and high-quality server hosting with over a dozen locations available across the world, ensuring low latency. After your purchase, the server is already configured and ready to use.

    Use the code **"TheNimbleNinja"** to get **25% OFF** your first month as a new client for any of their gaming servers.
