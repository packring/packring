---
Title: 'PixelBomb'
Author: 'maltesewarrior'
Description: 'Play Pixelmon in a completely different way!'
Image: 'pixelbomb.png'
URL: 'https://www.curseforge.com/minecraft/modpacks/pixelbomb'
Categories: ['exploration','pvp','hardcore','multiplayer','light']
---
![](https://www.bisecthosting.com/images/CF/Pixelbomb/BH_PB_HEADER.png)

Play Pixelmon in a completely different way! While playing PixelBomb you'll have to fend for yourself in the Zoza region where the Pixelmon have become the only source of food, other than plant life, and consequentially... Much more aggressive. Much like how the Pokemon Legends Arceus trailer is promising. Not only are the Pixelmon on edge, but there are tall angry purple beasts that seem to have a grudge, along with mutated monsters ravaging the land. This mod pack does have lore and more will be added along the way, try to figure out what happened in this region while you brave this dangerous place with your Pixelmon.

![](https://www.bisecthosting.com/images/CF/Pixelbomb/BH_PB_OVERVIEW.png)

The main goal of PixelBomb is truly to have fun! While there are lore elements and (mostly) frustrating mobs trying to destroy you the main aim of the game is to experience Pixelmon in a way it never has been before. Experience it in a more realistic setting where the Pixelmon are the cows, chickens, and pigs, but unlike the classic livestock the Pixelmon fight back (similarly to the Anime). Try to see if you can become a Pixelmon master in this disastrous region, will you give up, or will you be the best?

Modlist:

Spoiler (click to show)

*   [Progressive Bosses (by Insane96MCP)](https://www.curseforge.com/minecraft/mc-mods/progressive-bosses)
*   [Just Enough Items (JEI) (by mezz)](https://www.curseforge.com/minecraft/mc-mods/jei)
*   [Enchantment Descriptions (by DarkhaxDev)](https://www.curseforge.com/minecraft/mc-mods/enchantment-descriptions)
*   [Vanilla Boom (by phrille)](https://www.curseforge.com/minecraft/mc-mods/vanilla-boom)
*   [Pixelmon DX (by dumpsterfrog)](https://www.curseforge.com/minecraft/texture-packs/pixelmon-dx)
*   [MrCrayfish's Gun Mod (by MrCrayfish)](https://www.curseforge.com/minecraft/mc-mods/mrcrayfishs-gun-mod)
*   [Annahstas Beastrinia 32x (by Filmjolk)](https://www.curseforge.com/minecraft/texture-packs/annahstas-beastrinia-pokemon-inspired-32x)
*   [Bad Mobs (by DarkhaxDev)](https://www.curseforge.com/minecraft/mc-mods/bad-mobs)
*   [Pixelmon (by PixelmonMod)](https://www.curseforge.com/minecraft/mc-mods/pixelmon)
*   [MovingWorld (by darkevilmac)](https://www.curseforge.com/minecraft/mc-mods/movingworld)
*   [Pam's Weee! Flowers (by pamharvestcraft)](https://www.curseforge.com/minecraft/mc-mods/pams-weee-flowers)
*   [Spartan Weaponry (by ObliviousSpartan)](https://www.curseforge.com/minecraft/mc-mods/spartan-weaponry)
*   [MrCrayfish's Furniture Mod (by MrCrayfish)](https://www.curseforge.com/minecraft/mc-mods/mrcrayfish-furniture-mod)
*   [AppleSkin (by squeek502)](https://www.curseforge.com/minecraft/mc-mods/appleskin)
*   [The Spice of Life (by squeek502)](https://www.curseforge.com/minecraft/mc-mods/the-spice-of-life)
*   [GraveStone Mod (by henkelmax)](https://www.curseforge.com/minecraft/mc-mods/gravestone-mod)
*   [Davincis Vessels (by darkevilmac)](https://www.curseforge.com/minecraft/mc-mods/davincis-vessels)
*   [OreLib (by OreCruncher)](https://www.curseforge.com/minecraft/mc-mods/orelib)
*   [More Player Models (by Noppes\_)](https://www.curseforge.com/minecraft/mc-mods/more-player-models)
*   [DexNav for Pixelmon (by BeetoGuy)](https://www.curseforge.com/minecraft/mc-mods/dexnav-for-pixelmon)
*   [Bedspreads (Forge) (by TheIllusiveC4)](https://www.curseforge.com/minecraft/mc-mods/bedspreads)
*   [Sound Filters (by Tmtravlr)](https://www.curseforge.com/minecraft/mc-mods/sound-filters)
*   [Inspirations (by KnightMiner)](https://www.curseforge.com/minecraft/mc-mods/inspirations)
*   [Snow! Real Magic! ⛄ (by Snownee\_)](https://www.curseforge.com/minecraft/mc-mods/snow-real-magic)
*   [Ferdinand's Flowers (by c0\_0ry)](https://www.curseforge.com/minecraft/mc-mods/ferdinands-flowers)
*   [Epic Siege Mod (by Funwayguy)](https://www.curseforge.com/minecraft/mc-mods/epic-siege-mod)
*   [SwingThroughGrass (by exidex)](https://www.curseforge.com/minecraft/mc-mods/swingthroughgrass)
*   [Fish's Undead Rising (by fish0016054)](https://www.curseforge.com/minecraft/mc-mods/fishs-undead-rising)
*   [Antique Atlas (by Hunternif)](https://www.curseforge.com/minecraft/mc-mods/antique-atlas)
*   [AppleCore (by squeek502)](https://www.curseforge.com/minecraft/mc-mods/applecore)
*   [Biomes O' Plenty (by Forstride)](https://www.curseforge.com/minecraft/mc-mods/biomes-o-plenty)
*   [Fairy Lights (by pau101)](https://www.curseforge.com/minecraft/mc-mods/fairy-lights)
*   [Foam​Fix (by asiekierka)](https://www.curseforge.com/minecraft/mc-mods/foamfix-optimization-mod)
*   [Tree Chopper (by MrDuchy)](https://www.curseforge.com/minecraft/mc-mods/tree-chopper)
*   [MrCrayfish's Jumping Castle Mod (by MrCrayfish)](https://www.curseforge.com/minecraft/mc-mods/mrcrayfishs-jumping-castle-mod)
*   [First Aid (by ichttt)](https://www.curseforge.com/minecraft/mc-mods/first-aid)
*   [Dynamic Surroundings (by OreCruncher)](https://www.curseforge.com/minecraft/mc-mods/dynamic-surroundings)
*   [Illuminations 🔥 (by doctor4t)](https://www.curseforge.com/minecraft/mc-mods/illuminations)
*   [Throwable Explosives (by xBigEllx)](https://www.curseforge.com/minecraft/mc-mods/throwable-explosives)
*   [Death Compass (by vadis365)](https://www.curseforge.com/minecraft/mc-mods/death-compass)
*   [Mantle (by mDiyo)](https://www.curseforge.com/minecraft/mc-mods/mantle)
*   [LootTweaker (by Daomephsta)](https://www.curseforge.com/minecraft/mc-mods/loottweaker)
*   [Doggy Talents (by percivalalb)](https://www.curseforge.com/minecraft/mc-mods/doggy-talents)
*   [CraftTweaker (by Jaredlll08)](https://www.curseforge.com/minecraft/mc-mods/crafttweaker)
*   [YUNG's Better Caves (Forge) (by YUNGNICKYOUNG)](https://www.curseforge.com/minecraft/mc-mods/yungs-better-caves)
*   [The Farlanders (by ModdingLegacy)](https://www.curseforge.com/minecraft/mc-mods/farlanders)
*   [Set Home & Waypoints (by DodgeMan)](https://www.curseforge.com/minecraft/mc-mods/set-home-waypoints)
*   [Pixelmon Clothes (by CriogonalPlays)](https://www.curseforge.com/minecraft/mc-mods/pixelmon-clothes)
*   [Spartan Shields (by ObliviousSpartan)](https://www.curseforge.com/minecraft/mc-mods/spartan-shields)
*   [AtomicStryker's Infernal Mobs (by atomicstrykergrumpy)](https://www.curseforge.com/minecraft/mc-mods/atomicstrykers-infernal-mobs)
*   [Additional Structures (by XxRexRaptorxX)](https://www.curseforge.com/minecraft/mc-mods/additional-structures)
*   [Iron Backpacks (by gr8pefish)](https://www.curseforge.com/minecraft/mc-mods/iron-backpacks)
*   [LootBags (by Mina\_the\_Engineer)](https://www.curseforge.com/minecraft/mc-mods/lootbags)
*   [Obfuscate (by MrCrayfish)](https://www.curseforge.com/minecraft/mc-mods/obfuscate)
*   [Just Enough Resources (JER) (by way2muchnoise)](https://www.curseforge.com/minecraft/mc-mods/just-enough-resources-jer)
*   [Cosmetic Armor Reworked (by LainMI)](https://www.curseforge.com/minecraft/mc-mods/cosmetic-armor-reworked)
*   [YUNG's Better Mineshafts (Forge) (by YUNGNICKYOUNG)](https://www.curseforge.com/minecraft/mc-mods/yungs-better-mineshafts-forge)
*   [CreativeCore (by CreativeMD)](https://www.curseforge.com/minecraft/mc-mods/creativecore)
*   [DeathQuotes / Death Quotes (by lordcazsius)](https://www.curseforge.com/minecraft/mc-mods/deathquotes-death-quotes)
*   [Nature's Compass (by Chaosyr)](https://www.curseforge.com/minecraft/mc-mods/natures-compass)
*   [Comforts (Forge) (by TheIllusiveC4)](https://www.curseforge.com/minecraft/mc-mods/comforts)
*   [Fast Leaf Decay (by olafskiii)](https://www.curseforge.com/minecraft/mc-mods/fast-leaf-decay)
*   [MrCrayfish's Vehicle Mod (by MrCrayfish)](https://www.curseforge.com/minecraft/mc-mods/mrcrayfishs-vehicle-mod)
*   [Pam's HarvestCraft (by pamharvestcraft)](https://www.curseforge.com/minecraft/mc-mods/pams-harvestcraft)
*   [Placeable Items (by Ferdz\_)](https://www.curseforge.com/minecraft/mc-mods/placeable-items)
*   [Controlling (by Jaredlll08)](https://www.curseforge.com/minecraft/mc-mods/controlling)
*   [Ender Mail (by Chaosyr)](https://www.curseforge.com/minecraft/mc-mods/ender-mail)
*   [Pam's BrewCraft (by pamharvestcraft)](https://www.curseforge.com/minecraft/mc-mods/pams-brewcraft)
*   [Mutant Beasts (by Chumbanotz)](https://www.curseforge.com/minecraft/mc-mods/mutant-beasts)
*   [Statues (by ShyNieke)](https://www.curseforge.com/minecraft/mc-mods/statues)
*   [Hwyla (by TehNut)](https://www.curseforge.com/minecraft/mc-mods/hwyla)
*   [EnhancedVisuals (by CreativeMD)](https://www.curseforge.com/minecraft/mc-mods/enhancedvisuals)
*   [VanillaFix (by Runemoro)](https://www.curseforge.com/minecraft/mc-mods/vanillafix)

[![](https://cdn.discordapp.com/attachments/857630877378674728/888061738505887795/Pixelbomb-07.png)](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fwww.bisecthosting.com%252fmaltese)

If you want to play any of my magnificent modpacks with your friends or just want to support me, please consider using Bisect Hosting for all your server hosting needs! I personally use them and their services are close to excellent, they are what have made it possible for me to make enjoyable modpacks! Just go to [https://www.bisecthosting.com/maltese](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fwww.bisecthosting.com%252fmaltese) or click on the banner above and use the promo code: Maltese for 25% off your first month! If you can't afford a server, which I completely understand, there are preinstalled files in the server files that you can use to run a server, go catch 'em all!

![](https://www.bisecthosting.com/images/CF/Pixelbomb/BH_PB_COMMUNITY.png)

Check out my other modpack Originations and this awesome Pack ring I'm a part of and support the other amazing creators!

[![](https://cdn.discordapp.com/attachments/857630877378674728/860353281120141335/Originations-01.png)](https://www.curseforge.com/minecraft/modpacks/originations)[
![](https://cdn.discordapp.com/attachments/862061100933972038/865070244203855902/packring-icon.png)](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fpackring.gitlab.io%252fpackring%252f)[![](https://cdn.discordapp.com/attachments/857630877378674728/896419059132923925/BH_PB_DISCORD.png)](https://discord.gg/4QprmjND)

 I also have a discord server now, go join!!!!
