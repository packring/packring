---
Title: 'Project Equivalence New Horizon Two'
Author: 'thenimbleninja'
Description: 'An expert Skyblock Modpack'
Image: 'project-eqivavlence-new-horizon-two.png'
URL: 'https://www.curseforge.com/minecraft/modpacks/project-equivalence-new-horizon-two'
Categories: ['Skyblock', 'Magic', 'Quests', 'Tech' ]
---

![](https://www.bisecthosting.com/images/CF/Project_Equivalence/BH_PE_Header.png)

![](https://www.bisecthosting.com/images/CF/Project_Equivalence/BH_PE_Overview.png)

Project Equivalence New Horizons Two Is an expert Skyblock Modpack made in Minecraft !.16.5 while the original was made in Minecraft 1.15!

the start of the pack is basically the same as the original but instead of getting all the items in the form of the tome of knowledge you are forced to earn all the items with emc! your resource gen is exnihlo so as thats basicly the only resource generation mod up to date! quests will guide you through the pack. the tech tree is branching path. magic and tech are separate progression lines but they all lead to end game which will be creative items!

**current major issue with project e it makes worlds load longer and makes them fail to load but they will still load anyways so dont worry too much when it says that! nothing i can do to fix this as its not been updated for a long time**
