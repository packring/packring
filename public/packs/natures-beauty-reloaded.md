---
Title: 'Nature''s Beauty Reloaded'
Author: 'Jackboi03'
Description: '1.16.5 Vanilla+ Modpack Based on the Nature''s Beauty Pack'
Image: 'naturesbeauty.png'
URL: 'https://www.curseforge.com/minecraft/modpacks/natures-beauty-reloaded'
Categories: ['adventure', 'exploration', 'map-based', 'multiplayer']
Color: 345
---
![img](https://www.bisecthosting.com/images/CF/Natures_Beauty_Reloaded/BH_NBR_Header.png)![img](https://www.bisecthosting.com/images/CF/Natures_Beauty_Reloaded/BH_NBR_Features.png)

![img](https://www.bisecthosting.com/images/CF/Natures_Beauty_Reloaded/BH_NBR_Recommendations.png)

 

To get the best experience, please use Optifine and a shader of your choice!

 

[**Optifine Download**](https://www.curseforge.com/linkout?remoteUrl=https%3a%2f%2foptifine.net%2fdownloads)

**[Sildur Shader Download](https://www.curseforge.com/linkout?remoteUrl=https%3a%2f%2fsildurs-shaders.github.io%2fdownloads%2f)**

 

**![img](https://www.bisecthosting.com/images/CF/Natures_Beauty_Reloaded/BH_NBR_Highlights.png)**

Custom World Generation with Terraforge

A lot of little things that make Minecraft better, like Emotes, Chest Searching, Right click Armor to equip it, etc. with Quark!

Upgrade your tools with Tetra

Vanilla'ish Mini- and Worldmap with Antique Atlas

New biomes, structures, and mobs in the End Dimension

Starter Items with Guide

Random Structure Generation through Valhelsia Structures

New Biomes

New Fruits and Vegetables

New Animals

Pets that give you Effects

Sounds when you walk, equip a weapon, etc.

A Tombstone which stores all your Items

A pot in which you can plan anything

A ingame Camera with Images similar to paintings

Chisel a block into more tiny blocks and use them for building 

![img](https://www.bisecthosting.com/images/CF/Natures_Beauty_Reloaded/BH_NBR_Upcoming.png)Showcase Video

Custom made Mod

Tutorail World

Storyline (optional)

**[![img](https://www.bisecthosting.com/images/CF/Natures_Beauty_Reloaded/BH_NBR_Affiliated.png)](https://www.curseforge.com/linkout?remoteUrl=https%3a%2f%2fhate9.gitlab.io%2fpackring)**

**[![img](https://www.bisecthosting.com/images/CF/Natures_Beauty_Reloaded/BH_NBR_PromoCard.png)](https://www.curseforge.com/linkout?remoteUrl=https%3a%2f%2fbisecthosting.com%2fNBR25)**

**[![img](https://www.bisecthosting.com/images/CF/Natures_Beauty_Reloaded/BH_NBR_Discord.png)](https://discord.gg/AxmmNkJxgM)**

**![img](https://www.bisecthosting.com/images/CF/Natures_Beauty_Reloaded/BH_NBR_Screenshots.png)**

![img](https://media.forgecdn.net/attachments/347/127/2021-03-11_21.png)

![img](https://media.forgecdn.net/attachments/346/273/2021-03-08_12.png)

![img](https://media.forgecdn.net/attachments/346/272/2021-03-08_12.png)

![img](https://media.forgecdn.net/attachments/347/271/2021-03-11_21.png)![img](https://media.forgecdn.net/attachments/347/273/2021-03-11_21.png)![img](https://media.forgecdn.net/attachments/347/274/2021-03-11_21.png)![img](https://media.forgecdn.net/attachments/372/896/screenshot-2.PNG)![img](https://media.forgecdn.net/attachments/372/897/screenshot-3.PNG)[Sildur's Vibrant shaders v1.283 Extreme-Volumetric lighting Shader](https://www.curseforge.com/linkout?remoteUrl=https%3a%2f%2fsildurs-shaders.github.io%2fdownloads%2f) used for Screenshots
