---
Title: 'Cobbleblock'
Author: 'Kreezxil'
Description: 'A world of Cobble. A starting Geode. Interesting Quest Lines.'
Image: 'cobbleblock.png'
URL: 'https://www.curseforge.com/minecraft/modpacks/kreezcraft-presents-cobbleblock'
Categories: ['adventure', 'exploration', 'multiplayer', 'quests', 'skyblock']
---
[![Bisect Hosting](https://www.bisecthosting.com/images/logos/dark_text@1538x500.png)](https://www.curseforge.com/linkout?remoteUrl=https%3a%2f%2fbisecthosting.com%2fkreezxil)

If you want a server setup for the Any mod pack with zero effort, get a [server with BisectHosting](https://www.curseforge.com/linkout?remoteUrl=https%3a%2f%2fbisecthosting.com%2fkreezxil) and receive 25% off your first month as a new customer using the code kreezxil

 

## Description

After 1000s of years underground you awaken to find that apocalyptic land from whence you slumbered has long since gone and stabilized. You  regain your conscience and come to realize that you are in a living  geode. Or rather, you are the life inside the geode. Like the ancient  creator, you must create the basics of construction from the surrounding rock to produce ever more advanced tech.

 

Will you survive or go crazy as bat?!!

 

Patterned off Stoneblock I & II and built with mods with Refined  Storage as the mass storage mod of choice. Furthermore, OpenComputers.

 

## Videos



<iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?list=PLw2Hm59h2M7TmdQmbY-5YepqV9A8xVJ25" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Brought to you by MGlolenstine
