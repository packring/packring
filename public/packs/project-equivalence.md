---
Title: 'Project Equivalence'
Author: 'thenimbleninja'
Description: 'Project Equivalence is an expert modpack revolving around the ProjectE mod.'
Image: 'project-equivalence.png'
URL: 'https://www.curseforge.com/minecraft/modpacks/project-equivalence'
Categories: ['magic','quests','skyblock']
---
![](https://www.bisecthosting.com/images/CF/Project_Equivalence/BH_PE_Header.png)

![](https://www.bisecthosting.com/images/CF/Project_Equivalence/BH_PE_Overview.png)

Project Equivalence is an expert modpack revolving around the ProjectE mod. It’s conception was inspired by TheLimePixel’s pack, Equivalent Skies, in which you are started with a Transmutation Tablet and Tome of Knowledge. The goal of this pack, however, is to craft the Final Star from ProjectEX (an add-on to ProjectE). This will not be an easy task, as most of the recipes and EMC values for items have been changed. A quest line has been made using Better Quests to guide you through the pack properly. This modpack contains 200+ mods, and will need at least 6 GB of allocated RAM to run well. in the new version of the pack there is a new hard mode added via packmode!

![](https://www.bisecthosting.com/images/CF/Project_Equivalence/BH_PE_Community.png)

[![](https://i.imgur.com/4k8lKdV.png)](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fwww.guilded.gg%252fi%252fRkrdW7lE)

[![](https://i.imgur.com/uabSTGk.png)![](https://cdn.discordapp.com/attachments/862061100933972038/865086703261974598/packring-logo.png)](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fpackring.gitlab.io%252fpackring%252f)

![](https://www.bisecthosting.com/images/CF/Project_Equivalence/BH_PE_Multiplayer.png)

Project Equivalence has partnered with BisectHosting!
[![](https://www.bisecthosting.com/images/CF/Project_Equivalence/BH_PE_PromoCard.png)](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fbisecthosting.com%252fTheNimbleNinja)

**BisectHosting** offers quick, simple, and high-quality server hosting with over a dozen locations available across the world, ensuring low latency. After your purchase, the server is already configured and ready to use.

Use the code **"TheNimbleNinja"** to get **25% OFF** your first month as a new client for any of their gaming servers.
