---
Title: 'Originations'
Author: 'MalteseWarrior'
Description: 'Origins galore, magical properties, battle towers and more!'
Image: 'originations.png'
URL: 'https://www.curseforge.com/minecraft/modpacks/originations'
Categories: ['exploration', 'adventure', 'combat', 'magic', 'multiplayer']
---
![img](https://media.discordapp.net/attachments/857630877378674728/860353281045168128/Originations-02.png?width=929&height=208)

 

Welcome to my modpack Originations! I hope you have fun here!!

Here you can choose what type of experience you will have! Will you choose  to be born from the depths of the nether, swim through the endless seas  or soar through the skies?

Every experience comes with their own trials and tribulations, where do you  originate? (Put in the comments your favorite Origin!)

**I suggest a minimum of 5 gigabytes to run this modpack**

 

 

![img](https://cdn.discordapp.com/attachments/857630877378674728/860353282748579880/Originations-03.png)

Ok, so what exactly is Originations and why is it different (and possibly  better) than other Origin modpacks? Origination’s purpose is to enhance  the experience for every origin possible, so a lot of the world has been changed to suit whatever origin you choose, still accompanied by  challenges nonetheless! Such additions include underground villages if  your origin is more accustomed to being underground, an ocean-oriented  dimension for the fishy folk, and an expansive nether for those who burn for the excitement. Also, the worlds you will travel are imbued with  magic, maybe you can push the limits of your own limitations.  Nevertheless, this modpack is exploration heavy, find unlimited  structures with plenty of treasures, and do not miss out on the tower  guardians either! Just be prepared, the farther you travel the stronger  the monsters. (Mod list will be at the bottom of the page) Here is the  list of the origins here:

 

Human (If you're average like that)

Avian (Bird boy)

Arachnid (ewww)

Lepus (What’s up doc?)

Ghastling (What’s with the tears?)

Elytrian (It’s a move in the flight direction)

Shulk (You gotta open up sometimes)

Arcanite (I like your funny words magic man)

Snowman (Two eyes made of coal)

Creeper (Please don’t blow up my build)

Feline (A furry I see)

Enderian (What its just a pumpkin chill out!)

Dragonkin (The cooler elytrian)

Trashling (Picking this one will be a bit trashy)

Iron Golem (Different than Iron born I swear)

Tauros (I waved the red flag)

Goatlin (Baaaaaaaa)

Floran (What a hippie)

Monke (mmmmm)

Cursed (Well that sucks)

Weeping Menace (Imagine a robber just crying during a heist)

Infested (These really bug me)

Triton (This one krills me)

Hobbit (For you fantasy nerds out there)

Orc (A face only a mother could love)

Foxling (Swiper no swiping!)

Elf (Wait… like Santa?)

Dwarf (Dopey!!!)

Slimecican (You remind me of Gary)

Merling (Yes mermaids are part of your world)

Blazeborn (Just too hot to handle)

Boneling (Just keep them away from the dogs)

Axolotl (The first of your kind)

Cobblestonian (It's not just a boulder, it's a rock!)

Nukelian (Nuke to meet you too)

Astronos (You are astronomical)

Lava Sharkian (A hot sharkian)

Sharkian (The normal sharkian)

Lato (Daltogs go ew)

Daltog (Latos go ew)

Warden (You'll never see clearly now that you're blind)

Dimensional Being (You are a being, of dimensions)

Inchling (ha, short stack)

Piglin (Seems that you have sided with the enemy)

Phantom (Ya’ll not gonna sleep because of me!)

Zombiefied (You might want to go to the doctor)

Wyvern (How do you say it again?)

Sylvan (Magical hippie)

Golemborn (A literal Iron man)

Libra (The balance of the world is in your hands)
