---
Title: 'For Science!'
Author: 'HopsandBarley75'
Description: 'What if a pack had only science mods in it?'
Image: 'for-science.png'
URL: 'https://www.curseforge.com/minecraft/modpacks/for-science'
Categories: ['tech','exploration','quests','multipplayer']
---
**ATTENTION:  Itemphysic and Fluid Physics have been added to Version 1.08 of the pack.  I would love to hear your feedback on these mods:  Do you like them?  Would you tweak them?  Are there specific features of them you love or hate?**

**Storage Overhaul Mod is going to be removed in an upcoming patch (Probably 1.20).  Any items you have inside chests, barrels, storage barrels, or minecart chests (from that mod) at that time will most likely get destroyed/disappear upon loading into the world, after the mod is gone.  I advise that you switch to other storage options ASAP.**

**The mod is still in the pack, but all chests/barrels/and shulker boxes from the mod have been disabled and can no longer be crafted.**

****Any and all feedback is welcome and encouraged.  If I dont hear your suggestions, or bugs that you have discovered, I can't fix them or improve the game!  Please submit Bugs or Suggestions to my GitHub.  Feel free to post any general comments here.  I would love to hear some opinions on the pack.****

I am honored and excited to have been offered a BisectHosting Partnership!  The folks at BisectHosting have been very kind to me, and have offered me this great deal to any of you that would like to rent a server and play with your friends!  Just use my Promo Code and receive 25% off your first month!

[****![](https://www.bisecthosting.com/partners/custom-banners/71c99ebc-7459-4e62-9ac5-6819c27e3b37.png)****](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fbisecthosting.com%252fHOPS)

For Science!! is inspired by packs like [SevTech Ages](https://www.curseforge.com/minecraft/modpacks/sevtech-ages) and [Engineer's Life](https://www.curseforge.com/minecraft/modpacks/engineers-life). (Thanks, [Dreams01.](https://www.curseforge.com/members/dreams01/projects))  I wanted to make a quest-based tech pack, with custom recipes and custom production chains that feels rewarding, and encourages automation.

For Science! is focused around [Project Rankine](https://www.curseforge.com/minecraft/mc-mods/project-rankine) mod and the [**Create**](https://www.curseforge.com/minecraft/mc-mods/create) mod. and built for Minecraft 1.16.5. In this pack, you will start out with nothing but your quest book. Work you way from sticks and stones, up through cogs and shafts, all the way to lasers and Tungsten-Super alloy tools.

On your way, you will use the Create mod, and it's infinite capabilities, to create fantastic and ridiculous contraptions, to help make your life easier.

Experiment with Genetics.  Find wild animals, discover their traits, and breed them together, to produce optimal species of cows, sheet, pigs, rabbits, and llamas, depending on what your goals are.

You will also dip your toes into chemistry.  Gather as many different elements are you can find, or create.  Use these elements to create custom alloys, and experiment with them, to find out how slight tweaks in chemical composition affects the tools and weapons that you make from them.

There are several new villager professions, and dozens (and dozens and dozens) of new trades to be discovered, including the Wandering Trader.

Along the way, utilize  [**Farmer's Delight**](https://www.curseforge.com/minecraft/mc-mods/farmers-delight), [**Builders Crafts & Additions**](https://www.curseforge.com/minecraft/mc-mods/buildersaddition), [**Sophisticated Backpacks**](https://www.curseforge.com/minecraft/mc-mods/sophisticated-backpacks), and several other quality of life mods, to spruce up your builds, or simply make your life easier.

Sharpen your axe, and get out your sturdiest hammer.  You need them to cut wood and break stone.  A simple crafting grid doesn't do magic anymore.  You will rely heavily on your tools until you can advance into the world of Create, where automating all of the tedious processes will take your imagination to new heights.

In addition, there is plenty of exploration to do.  Also in the pack are YUNG's [Better Caves](https://www.curseforge.com/minecraft/mc-mods/yungs-better-caves), [Better Mineshafts](https://www.curseforge.com/minecraft/mc-mods/yungs-better-mineshafts-forge), [Better Portals](https://www.curseforge.com/minecraft/mc-mods/yungs-better-portals), and [Better Strongholds](https://www.curseforge.com/minecraft/mc-mods/yungs-better-strongholds).  Make sure you are well prepared, because each mod adds a bevy of new challenges.

There is an in-depth quest book to guide you through the pack, containing hundreds of quests.  But it is purely a guide, if you need it.  Experienced players are more than welcome to throw it away, and just play.  Progression is only gated behind the technology available to you at the time.

Future Plans:

\* Transportation mods

\*Rewards / Currency system

\*High Tech / Power Mods

\*New, Custom Machines (Maybe?  /shrug)

I really want to thank [DOCTORZED](https://www.curseforge.com/members/d0ctorzed/projects) (The Wizard), and [Czbuendel](https://www.curseforge.com/members/czbuendel/projects) for all of their help with the countless questions, bouncing off of ideas, and CraftTweaker insight that they provided to me.

MODLIST:
Thank You to all of these ridiculous creators for all of the hard work they put into their mods.

Spoiler (click to show)

[**Additional Lights**](https://www.curseforge.com/minecraft/mc-mods/additional-lights) **:** **(by [Mgen256](https://www.curseforge.com/members/mgen256/projects))**

**[Additional Lanterns](https://www.curseforge.com/minecraft/mc-mods/additional-lanterns) : (by [SuperMartijn642](https://www.curseforge.com/members/supermartijn642/projects))**

**[Advancement Plaques](https://www.curseforge.com/minecraft/mc-mods/advancement-plaques) :  (by [Grend\_G](https://www.curseforge.com/members/grend_g/projects))**

**[Advanced Shulkerboxes](https://www.curseforge.com/minecraft/mc-mods/advanced-shulkerboxes) : (by [henkelmax](https://www.curseforge.com/members/henkelmax/projects))**

[**AppleSkin**](https://www.curseforge.com/minecraft/mc-mods/appleskin) **: (by [Squeek502](https://www.curseforge.com/members/squeek502/projects))**

[**Architectury**](https://www.curseforge.com/minecraft/mc-mods/architectury-forge) **: (by [shedaniel](https://www.curseforge.com/members/shedaniel/projects))**

[**Better Advancements**](https://www.curseforge.com/minecraft/mc-mods/better-advancements) **: (by [way2muchnoise](https://www.curseforge.com/members/way2muchnoise/projects))**

[**YUNG's Better Caves**](https://www.curseforge.com/minecraft/mc-mods/yungs-better-caves)** : (by [YUNGNICKYOUNG](https://www.curseforge.com/members/yungnickyoung/projects))**

[**YUNG's Better Mineshafts**](https://www.curseforge.com/minecraft/mc-mods/yungs-better-mineshafts-forge) **: (by [YUNGNICKYOUNG](https://www.curseforge.com/members/yungnickyoung/projects))**

**[YUNG's Better Portals](https://www.curseforge.com/minecraft/mc-mods/yungs-better-portals) :  (by [YUNGNICKYOUNG](https://www.curseforge.com/members/yungnickyoung/projects))**

**[YUNG's Better Strongholds](https://www.curseforge.com/minecraft/mc-mods/yungs-better-strongholds) :  (by [YUNGNICKYOUNG](https://www.curseforge.com/members/yungnickyoung/projects))**

[**YUNG's Extras :****(by**](https://www.curseforge.com/minecraft/mc-mods/yungs-extras) **[YUNGNICKYOUNG](https://www.curseforge.com/members/yungnickyoung/projects))**

[**Builder's Crafts & Additions**](https://www.curseforge.com/minecraft/mc-mods/buildersaddition) **: (by [MRHminer](https://www.curseforge.com/members/mrhminer/projects))**

[**Ceramic Shears**](https://www.curseforge.com/minecraft/mc-mods/ceramic-shears) **: (by [thecech12](https://www.curseforge.com/members/thecech12/projects))**

**[Charm Reforged](https://www.curseforge.com/minecraft/mc-mods/charm-reforged) : (by [Svenhjol](https://www.curseforge.com/members/svenhjol/projects) )**

[**Clumps**](https://www.curseforge.com/minecraft/mc-mods/clumps) **: (by [JaredIII08](https://www.curseforge.com/members/jaredlll08/projects))**

[**Collective**](https://www.curseforge.com/minecraft/mc-mods/collective) **: (by [Serilum](https://www.curseforge.com/members/serilum/projects) )**

[**Comforts **](https://www.curseforge.com/minecraft/mc-mods/comforts)**[ ](https://www.curseforge.com/minecraft/mc-mods/comforts): (by [TheIllusiveC4](https://www.curseforge.com/members/theillusivec4/projects) )**

[**Controlling**](https://www.curseforge.com/minecraft/mc-mods/controlling) **: (by [JaredIII08](https://www.curseforge.com/members/jaredlll08/projects))**

[**Crafting Tweaks**](https://www.curseforge.com/minecraft/mc-mods/crafting-tweaks) **: (by [TheNinth](https://www.curseforge.com/members/blaytheninth/projects) )**

[**CraftTweaker**](https://www.curseforge.com/minecraft/mc-mods/crafttweaker) **: (by [JaredIII08 ](https://www.curseforge.com/members/jaredlll08/projects))**

[**Create Crafts & Additions**](https://www.curseforge.com/minecraft/mc-mods/createaddition) **: (by [MRHminer](https://www.curseforge.com/members/mrhminer/projects))**

[**Create-MC**](https://www.curseforge.com/minecraft/mc-mods/create) **: (by [simibubi](https://www.curseforge.com/members/simibubi/projects) )**

[**CreateTweaker**](https://www.curseforge.com/minecraft/mc-mods/createtweaker) **: (by [JaredIII08 ](https://www.curseforge.com/members/jaredlll08/projects))**

**[CreativeCore](https://www.curseforge.com/minecraft/mc-mods/creativecore) :  ( by [CreativeMD](https://www.curseforge.com/members/creativemd/projects))**

[**Decorative Blocks**](https://www.curseforge.com/minecraft/mc-mods/decorative-blocks) **: (by [stohun](https://www.curseforge.com/members/stohun/projects) )**

[**Farmer's Delight**](https://www.curseforge.com/minecraft/mc-mods/farmers-delight) **: (by [vectorwing](https://www.curseforge.com/members/vectorwing/projects) )**

[**Farmer's Tea**](https://www.curseforge.com/minecraft/mc-mods/farmers-tea) **: (by [beautiful\_developer](https://www.curseforge.com/members/beautiful_developer/projects) )**

[**Floor Mats**](https://www.curseforge.com/minecraft/mc-mods/floor-mats) **: (by [DOCTORZED](https://www.curseforge.com/members/d0ctorzed/projects) )**

**[Fluid Physics](https://www.curseforge.com/minecraft/mc-mods/fluid-physics-forge) :  (by [LolHens](https://www.curseforge.com/members/lolhens/projects))**

****[Flywheel](https://www.curseforge.com/minecraft/mc-mods/flywheel) : (by [jozufozu](https://www.curseforge.com/members/jozufozu/projects))****

[**ForageCraft**](https://www.curseforge.com/minecraft/mc-mods/foragecraft) **: (by [Jonathing](https://www.curseforge.com/members/jonathing/projects) )**

[**FTB Library**](https://www.curseforge.com/minecraft/mc-mods/ftb-library-forge) **: (by [LatvianModder](https://www.curseforge.com/members/latvianmodder/projects) )**

**FTB-Quests** **:** **(by [LatvianModder](https://www.curseforge.com/members/latvianmodder/projects) )**

**[Geckolib](https://www.curseforge.com/minecraft/mc-mods/geckolib) **** : (by [ThanosGecko](https://www.curseforge.com/members/thanosgecko/projects))**

[**Genetic Animals**](https://www.curseforge.com/minecraft/mc-mods/genetic-animals) **: (by [mokiyoki](https://www.curseforge.com/members/mokiyoki/projects) )**

[**Gravestone Mod**](https://www.curseforge.com/minecraft/mc-mods/gravestone-mod) **: (by [henkelmax](https://www.curseforge.com/members/henkelmax/projects) )**

**[Guard Villagers](https://www.curseforge.com/minecraft/mc-mods/guard-villagers) : (by [allmightytallestred](https://www.curseforge.com/members/almightytallestred/projects) )**

[**Item Filters**](https://www.curseforge.com/minecraft/mc-mods/item-filters-forge) **: (by [LatvianModder](https://www.curseforge.com/members/latvianmodder/projects) )**

**[ItemPhysic](https://www.curseforge.com/minecraft/mc-mods/itemphysic) : ( by [CreativeMD](https://www.curseforge.com/members/creativemd/projects))**

**[Jade](https://www.curseforge.com/minecraft/mc-mods/jade) : (by [Snownee\_](https://www.curseforge.com/members/snownee_/projects) )**

[**JEI**](https://www.curseforge.com/minecraft/mc-mods/jei) **: (by [mezz](https://www.curseforge.com/members/mezz/projects) )**

[**JEI Integration**](https://www.curseforge.com/minecraft/mc-mods/jei-integration) **: (by [SnowShock35](https://www.curseforge.com/members/snowshock35/projects) )**

[**JEITweaker**](https://www.curseforge.com/minecraft/mc-mods/jeitweaker) **: (by [JaredIII08 ](https://www.curseforge.com/members/jaredlll08/projects))**

**[Masterful Machinery](https://www.curseforge.com/minecraft/mc-mods/masterful-machinery) : ([TicTicBoooom](https://www.curseforge.com/members/ticticboooom/projects))**

**[More Overlays Updated](https://www.curseforge.com/minecraft/mc-mods/more-overlays-updated) : (by [RiDGo8](https://www.curseforge.com/members/ridgo8/projects))**

[**Mouse Tweaks**](https://www.curseforge.com/minecraft/mc-mods/mouse-tweaks) **: (by [YaLTeR )](https://www.curseforge.com/members/yalter/projects)**

[**OpenLoader**](https://www.curseforge.com/minecraft/mc-mods/open-loader) **: (by [DarkhaxDev](https://www.curseforge.com/members/darkhaxdev/projects) )**

[**Patchouli**](https://www.curseforge.com/minecraft/mc-mods/patchouli) **: (by [Vazkii](https://www.curseforge.com/members/vazkii/projects) )**

[**Pane In The Glass**](https://www.curseforge.com/minecraft/mc-mods/pane-in-the-glass) **: (by [MongoTheElder](https://www.curseforge.com/members/mongotheelder/projects) )**

**[Path Under Gates](https://www.curseforge.com/minecraft/mc-mods/path-under-gates) : (by [cyberslas](https://www.curseforge.com/members/cyberslas/projects) )**

[**Project Rankine**](https://www.curseforge.com/minecraft/mc-mods/project-rankine) **: (by [cannolicatfish](https://www.curseforge.com/members/cannolicatfish/projects) )**

[**Rapid Leaf Decay**](https://www.curseforge.com/minecraft/mc-mods/rapid-leaf-decay) **: (by [GeheimagentNr1](https://www.curseforge.com/members/geheimagentnr1/projects) )**

**[Regrowth](https://www.curseforge.com/minecraft/mc-mods/regrowth) : (by [MacTso](https://www.curseforge.com/members/mactso/projects))**

[**Respawning Shulkers**](https://www.curseforge.com/minecraft/mc-mods/respawning-shulkers) **: (by [Serilum](https://www.curseforge.com/members/serilum/projects) )**

**[Scalable Cats Force](https://www.curseforge.com/minecraft/mc-mods/scalable-cats-force) :  (by [Kotori316](https://www.curseforge.com/members/kotori316/projects))**

[**Serene Seasons**](https://www.curseforge.com/minecraft/mc-mods/serene-seasons) **: (by [TheAdubbz](https://www.curseforge.com/members/theadubbz/projects) )**

**[Sewing Kit](https://www.curseforge.com/minecraft/mc-mods/sewing-kit) : (by [gigaherz](https://www.curseforge.com/members/gigaherz/projects))**

[**Shulker Drops Two**](https://www.curseforge.com/minecraft/mc-mods/shulker-drops-two) **: (by [Serilum](https://www.curseforge.com/members/serilum/projects) )**

[**Simply Tea**](https://www.curseforge.com/minecraft/mc-mods/simply-tea) **: (by [EpicSquid315](https://www.curseforge.com/members/epicsquid315/projects) )**

[**Sophisticated Backpacks**](https://www.curseforge.com/minecraft/mc-mods/sophisticated-backpacks) **: (by [P3pp3rF1y](https://www.curseforge.com/members/p3pp3rf1y/projects) )**

**[Spare Parts](https://www.curseforge.com/minecraft/mc-mods/spare-parts): (by [DOCTORZED](https://www.curseforge.com/members/d0ctorzed/projects) )**

[**Starter Kit**](https://www.curseforge.com/minecraft/mc-mods/starter-kit) **: (by [Serilum](https://www.curseforge.com/members/serilum/projects) )**

[**Storage Overhaul**](https://www.curseforge.com/minecraft/mc-mods/storage-overhaul) **: (by [henkelmax](https://www.curseforge.com/members/henkelmax/projects) )**

**Supermartijn's 642 Core Lib : **

[**SwingThroughGrass**](https://www.curseforge.com/minecraft/mc-mods/swingthroughgrass) **: (by [exidex](https://www.curseforge.com/members/exidex/projects) )**

[**Tool Stats**](https://www.curseforge.com/minecraft/mc-mods/tool-stats) **: (by [DarkhaxDev](https://www.curseforge.com/members/darkhaxdev/projects) )**

[**Trash Slot**](https://www.curseforge.com/minecraft/mc-mods/trashslot) **: (by [BlayTheNinth](https://www.curseforge.com/members/blaytheninth/projects) )**

[**UntitledDuckMod**](https://www.curseforge.com/minecraft/mc-mods/untitled-duck-mod-forge) **: (by [Paspartout](https://www.curseforge.com/members/paspartout/projects) )**

[**Villager Names**](https://www.curseforge.com/minecraft/mc-mods/villager-names) **: (by [Serilum](https://www.curseforge.com/members/serilum/projects) )**

**[Wily Textiles](https://www.curseforge.com/minecraft/mc-mods/wily-textiles) : ( [MCThe\_Paragon](https://www.curseforge.com/members/mcthe_paragon/projects))**

[**Wooden Buckets**](https://www.curseforge.com/minecraft/mc-mods/wooden-buckets) **: (by [RedLikeRose0](https://www.curseforge.com/members/redlikerose0/projects) )**

[**Xaero's Minimap**](https://www.curseforge.com/minecraft/mc-mods/xaeros-minimap) **: (by [xaero96](https://www.curseforge.com/members/xaero96/projects) )**

[**Xaero's World Map**](https://www.curseforge.com/minecraft/mc-mods/xaeros-world-map) **: (by [xaero96](https://www.curseforge.com/members/xaero96/projects) )**

[**YungsAPI**](https://www.curseforge.com/minecraft/mc-mods/yungs-api) **:  (by [YUNGNICKYOUNG](https://www.curseforge.com/members/yungnickyoung/projects))**
