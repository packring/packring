---
Title: 'Survive Create Prosper 4'
Author: 'Griefed'
Description: 'Survive in a world filled with magic and wonder'
Image: 'scp_4.gif'
URL: 'https://www.curseforge.com/minecraft/modpacks/survive-create-prosper-4'
Categories: ['tech','magic','exploration','multiplayer']
---
![VoxelArt](https://i.griefed.de/images/2021/01/18/scp4_voxel.gif)
(Voxelart by the wonderful and beautiful [SayuriRosa](https://twitter.com/Pondworth/status/1341556367346249729). Not representative of actual gameplay and or graphics.)

![banner1](https://i.griefed.de/images/2020/12/01/banner_survive_tall.png)
![banner2](https://i.griefed.de/images/2020/12/01/banner_create_tall089730c47cb3691f.png)
![banner3](https://i.griefed.de/images/2020/12/01/banner_prosper_tall.png)

**Survive Create Prosper 4 offers a variety of mods which add new content, functionality, experiences and enhance the experience you're used to from vanilla Minecraft.**

**Survive Create Prosper aims to expand the world with technology, magic, exploration and adventure without outstaying its welcome.**

**Biomes O'Plenty, Oh The Biomes You Will Go To, Traverse and more mods expand the world of Minecraft with beautiful new biomes to explore.**

**Applied Energistics, Immersive Engineering / Petroleum / Railroading, Extreme Reactors, SecurityCraft and more mods aim to satisfy your technologie needs whilst staying immersive.**

**Botania, Blood Magic, Psi and other mods aim to quell your hunger for magical prowess.**

**A variety of useful, decorative or cool mods like Artifacts, Camera Mod, Cooking for Blockheads, Pam's Harvestcraft, Macaw's mods, Mystical Agriculture and addons, Paintings++ and lots of other mods add to the world, so you will always have something to explore, build, decorate or create.**

**That's just a small example of what's included in SCP. So go ahead and give it a try! See the modlist below to see whether there's something of interest for you!**

**Looking for a world to get started with Survive Create Prosper 4? Look no further, try [Survive Create Prosper 4 Spawn Island](https://www.curseforge.com/minecraft/worlds/survive-create-prosper-4-spawn-island)!**
