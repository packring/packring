---
Title: 'Scientific Dreams'
Author: 'thenimbleninja'
Description: 'An expert modpack all about tech'
Image: 'scientists-dream.png'
URL: 'https://www.curseforge.com/minecraft/modpacks/scientists-dream'
Categories: ['tech','quests','skyblock']
---

![](https://www.bisecthosting.com/images/CF/Scientific_Dreams/BH_SD_Overview.png)

Scientists Dream Is an expert modpack all about tech with the added bonus of having project e for endgame item duplication.Scientists dream will have multiple different world types to choose from  to start your journey.. Resource generation for the beginning is a custom resource gen then progresses into mystical agriculture and chickens, but chickens don't just lay raw resources they lay eggs of the resource the chicken used to lay. You then turn the resource eggs into the raw resource in a modular machine. Similar with mystical agriculture you take the essences you get from the crops and turn them into raw resources via a modular machine. Each tier of crop has its own tier of machine. The modpack will have quests to help you progress. End game for the pack is avarita and project e. I've spent some time making the progress in my liking. Each machine frame is made in the compact machines compactor so be prepared for that!

 ![](https://www.bisecthosting.com/images/CF/Scientific_Dreams/BH_SD_Community.png)
[![Discord Server:https://discord.gg/KN8mx75](https://i.imgur.com/4k8lKdV.png)](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fwww.guilded.gg%252fi%252fRkrdW7lE)

 [![](https://i.imgur.com/uabSTGk.png)](https://www.patreon.com/NinjaTheNimble)

[![](https://cdn.discordapp.com/attachments/862061100933972038/865086703261974598/packring-logo.png)](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fpackring.gitlab.io%252fpackring%252f)

![](https://www.bisecthosting.com/images/CF/Scientific_Dreams/BH_SD_Multiplayer.png)

[![https://bisecthosting.com/TheNimbleNinja](https://www.bisecthosting.com/images/CF/Scientific_Dreams/BH_SD_PromoCard.png)](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fbisecthosting.com%252fTheNimbleNinja)

**   BisectHosting** offers quick, simple, and high-quality server hosting with over a dozen locations available across the world, ensuring low                                                     latency. After your purchase, the server is already configured and ready to use.

                     Use the code **"TheNimbleNinja"** to get **25% OFF** your first month as a new client for any of their gaming servers.
