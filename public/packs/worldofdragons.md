---
Title: 'World of Dragons'
Author: 'Kreezxil'
Description: 'Dragons, Magic, Technologies, Quests, Minecolonies, Mine & Slash'
Image: 'wod.png'
URL: 'https://www.curseforge.com/minecraft/modpacks/world-of-dragons'
Categories: ['adventure', 'combat', 'magic', 'tech', 'quests']
---
[![If you want a server setup for the  World of Dragons mod pack with zero effort, get a server with BisectHosting and receive  25% off your first month as a new customer using the code kreezxil](https://media.forgecdn.net/attachments/316/265/bh_banner.png)](https://kreezcraft.com/wod)

[If you want a server setup for the **World of Dragons** mod pack with zero effort, get a server with BisectHosting and receive **25% off** your first month as a new customer using the code **kreezxil**](https://kreezcraft.com/wod)

 

![img](https://media.forgecdn.net/attachments/316/266/description.png)

 

## Welcome to the **World of Dragons** adventurer!

 

In this world of technology and magic it is your quest to subdue the dragons, using
any combination of technology and magic you so desire. To help you in your quest there
are special armors that you can craft from the carcasses of the dragons you kill. You
may also encounter villages with a questing shopkeeper and village lords that have
special needs. Each has good rewards should you appease them.

 

## Go forth and conquer those Dragons!

 

![img](https://media.forgecdn.net/attachments/316/267/nitty_gritty.png)

 **World of Dragons** is built around [Ice and Fire](https://www.curseforge.com/minecraft/mc-mods/ice-and-fire-dragons) and heavily modified by [Mine and Slash](https://www.curseforge.com/minecraft/mc-mods/mine-and-slash-reloaded). Therefore, running into the wilderness to find that perfect spot to build your base is **immediately** the first wrong thing you can do. The further out you go from spawn the tougher the aggressive mobs become. The advantage is that as you kill  mobs they will drop gear and spells. Combine that with **Mine and Slash (MS)** experience you get from them to level up your gear, your spells, and  your skills. There is also a level gap, just like in many MMORPG FANTASY games which means that fighting the strongest thing you can find will  not net you a metric ton of experience to boost your levels really fast. It also means that grinding things that you find easy to kill will net  you less and less experience. Adventuring is your only solution to that  dilemma. The game will tell you if you're fighting monsters that are too low in level or too high in level.

 

 Because of **MS** solo play is not recommended. Why? **The mod scales monsters hp and attack strengths**. The higher the level the more hp and attack power they have. That means if you run out to find that perfect spot to build your base or to start your colony, a lowly pig or zombie can easily one-hit you! You've been  warned. 

 

 **Can you solo play regardless?** Yes, yes you can. At its heart **World of Dragons** is a kitchen sink pack and as such I have included a lot of magic and  technology mods. However, because I would rather you played it by  adventuring I have added things that making mining more difficult such  as divergent underground. Diamonds drop as chunks that you have to  combine with a chisel to refine. Ores that can't see air are harder than stone, when it can see sky it is softer which makes it easier to mine.  Not leveling your character will have consequences in regard to the  weapons and magic you craft. Put simply, if you don't level your  character, whatever you craft will not level either.

 

 **Therefore**, remember that **Mine & Slash Reloaded** is the governing authority. If you follow the alternative magic or  technology routes you will have a long road ahead of you, probably  longer than SevTech Ages or similar because to get the ore and stuff  you need to follow those development trees which means that you won't be leveling in MS and the monsters will regardless of your desire to be a  mad scientist or mage. At some point while collecting your ores and  doodads from the other mods, some zombie is going to 1 hit kill you  because you entirely ignored the adventure aspect of this pack.

 

 **I do not recommend** being base scientist or a crazy potion brewing witch unless you have  some friends who are out adventuring and then only because of your  obsession with a mod in the pack, maybe you can provide an unforeseen  edge to the boss fights. This is very rare. Instead, balance what you do in the base with exploration and combat.

 

## **OPTIFINE IS NOT DIRECTLY SUPPORTED USAGE MAY VARY**

 

## **RECOMMENDED RAM IS 6 GB**
