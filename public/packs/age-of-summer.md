---
Title: 'Age of Summer'
Author: 'Kreezxil'
Description: 'In an age of dragons, fresh out of the age of magic'
Image: 'age-of-summer.png'
URL: 'https://www.curseforge.com/minecraft/modpacks/age-of-summer'
Categories: ['adventure','exploration','magic','multiplayer','tech']
---
![](https://i.ibb.co/HqwG8Pr/Dragon-Header.png)

In an age of dragons, fresh out of the age of magic and the mysterious never ending blizzard, comes a new tale of world conquering. The land has been renewed, the sun is out and fresh air is in your lungs! And the dragons rebel yet once more. Are you up to the task of taming them all? Are you prepared for the new lightning dragon?

![](https://i.ibb.co/RHby2WL/Dragons-Features.png)

Spoiler (click to show)

**\* dragons, cyclops, sirens, no eternal winter**

**\* better caves and mineshafts**
**\* minecolonies**

**\* rpg hud**

**\* project mmo**

**\* mass storage (ae2, refined storage, simply storage networking, storage drawers)**

**\* computer craft**

**\* mobs hard af (champions)**

**\* lots of decoration mods (build your heart out)**

**\* We got dank!**

**\* dungeons, lots of dungeons**

**\* blocks that erode into other blocks in water**

**\* ftb mods (all)**

**\* hydrogel, iron chests and iron jetpacks**

****\* and so much more!****

[![](https://www.lduhtrp.net/image-100472236-14087903)](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fwww.kqzyfj.com%252fclick-100472236-14087903)

![](https://i.imgur.com/zLKINNq.png)

**BisectHosting offers simple, high quality server with a dozen of locations, ensuring low latency. Choose from their wide variety of hosting services, from mc hosting to web or even dedicated servers for a very affordable price point. The server is already configured for your ease after your purchase. **

[![](https://i.ibb.co/027xqP6/Copy-of-Copy-of-Copy-of-Cosmic-Banner.png)](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fwww.bisecthosting.com%252fkreezxil)

Use my promo code "**kreezxil**' to receive a 25% discount of your first month when buying any of their hosting services!

![](https://i.imgur.com/zLKINNq.png)

[![packring](https://i.imgur.com/1ah07M7.png)](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fpackring.gitlab.io%252fpackring%252f)
