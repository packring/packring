---
Title: 'Skulk Horde Survival'
Author: 'GRMikeatron'
Description: 'Immerse yourself in a battle for survival against the relentless Sculk Horde.'
Image: 'skulk-horde-survival.png'
URL: 'https://www.curseforge.com/minecraft/modpacks/sculk-horde-survival'
Categories: ['light','adventure','combat','exploration']
---

[**![Bisect Hosting. Use code Mikeatron for 25% off your first purchase.](https://www.bisecthosting.com/images/CF/Sculk_Horde/BH_Sculk_Horde_Promo.webp)**](https://bisecthosting.com/mikeatron)

![Skulk Horde Survival](https://www.dropbox.com/scl/fi/461bivphojd3i9r49bdjb/cusrforgepage.webp?rlkey=3dib48cez6zkp2rv3cvccs4sy&dl=1)

![Featured Mods](https://www.dropbox.com/scl/fi/y6g4a2n7gyg8pctnilguv/cusrforgepage2.webp?rlkey=2exo9uwwyihcenr1doyyfj4na&dl=1)

## ![Screenshots (label)](https://www.bisecthosting.com/images/CF/Sculk_Horde/BH_Sculk_Horde_Screenshots.webp)

*Note: These screenshots contain mobs from [Guard Villagers](https://www.curseforge.com/minecraft/mc-mods/guard-villagers) and villages from [Better Villages.](https://www.curseforge.com/minecraft/mc-mods/better-village-forge)*

![Screenshots](https://www.dropbox.com/s/3tqksmtu8s12rgg/screenshots.png?dl=1)
