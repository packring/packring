---
Title: 'Disunity Skyblock'
Author: 'LWLegends'
Description: 'Magic and Tech based Sky Block Survival'
Image: 'disunity.png'
URL: 'https://www.curseforge.com/minecraft/modpacks/disunity-skyblock'
Categories: ['skyblock','magic','multiplayer','quests','light']
---

**\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_**

***Best New Skyblock on CurseForge!***

***![](https://media.forgecdn.net/attachments/378/622/lsmpatreonbanner1.png)***

***Disunity Skyblock*** is a challenging and exciting, Magic and Tech based Sky Block Survival with Hundreds of Custom Quests and Rewards. Start on a ***Custom Built** **Island*** and explore many new mods and many of your old favorites combined to provide an experience like you've never seen!

*   ***Custom Spawn!***
*   ***Custom Islands!***
*   ***Custom Recipes!***
*   ***Challenging Quests!***
*   ***And Soooooo Much More!***

***\-----------------------------------------------------------------------------------------------------***

***\*\*Updated regularly\*\****

***\-----------------------------------------------------------------------------------------------------***

>  ****Reminiscent of the Agrarian Skies and Sky Factory Packs ~ *but in Minecraft v1.16.5!*****

****![](https://media.forgecdn.net/attachments/thumbnails/362/94/310/172/2021-05-07_22.png)********![](https://media.forgecdn.net/attachments/thumbnails/362/93/310/172/2021-05-07_22.png)![](https://media.forgecdn.net/attachments/thumbnails/362/92/310/172/2021-05-07_22.png)****![](https://media.forgecdn.net/attachments/360/220/2021-04-30_14.png)

[![](https://media.forgecdn.net/attachments/362/238/patronbutton.png)](https://www.patreon.com/bePatron?u=55271169)

***PUBLIC TEST SERVER IP:  135.125.149.241:25565 ***

 ![](https://media.forgecdn.net/attachments/361/541/disunity-skies.png)

**Come join the fun!**

[**![](https://media.forgecdn.net/attachments/362/29/discordlogo1.png)**](https://discord.gg/bexuaUpkUx)

[**![](https://www.bisecthosting.com/partners/custom-banners/4f585742-43a0-42fb-bb7f-a5f032175ee9.png)**](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fwww.bisecthosting.com%252flegends)

**[![](https://media.forgecdn.net/attachments/378/596/1ah07m7.png)](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fhate9.gitlab.io%252fpackring%252f)
**

**ModList**

*   [Bookshelf (by DarkhaxDev)](https://www.curseforge.com/minecraft/mc-mods/bookshelf)
*   [Just Enough Items (JEI) (by mezz)](https://www.curseforge.com/minecraft/mc-mods/jei)
*   [Custom Starter Gear (by brandon3055)](https://www.curseforge.com/minecraft/mc-mods/custom-starter-gear)
*   [Refined Storage Addons (by raoulvdberge)](https://www.curseforge.com/minecraft/mc-mods/refined-storage-addons)
*   [Mystical Agradditions (by BlakeBr0)](https://www.curseforge.com/minecraft/mc-mods/mystical-agradditions)
*   [Ex Nihilo: Sequentia (by NovaMachina)](https://www.curseforge.com/minecraft/mc-mods/ex-nihilo-sequentia)
*   [Inventory Pets (by Purplicious\_Cow\_)](https://www.curseforge.com/minecraft/mc-mods/inventory-pets)
*   [Pam's HarvestCraft 2 - Food Extended (by pamharvestcraft)](https://www.curseforge.com/minecraft/mc-mods/pams-harvestcraft-2-food-extended)
*   [Ender Storage 1.8.+ (by covers1624)](https://www.curseforge.com/minecraft/mc-mods/ender-storage-1-8)
*   [JEITweaker (by Jaredlll08)](https://www.curseforge.com/minecraft/mc-mods/jeitweaker)
*   [Powah! (by owmii)](https://www.curseforge.com/minecraft/mc-mods/powah)
*   [Storage Drawers (by Texelsaur)](https://www.curseforge.com/minecraft/mc-mods/storage-drawers)
*   [Industrial Foregoing (by Buuz135)](https://www.curseforge.com/minecraft/mc-mods/industrial-foregoing)
*   [Lollipop (by owmii)](https://www.curseforge.com/minecraft/mc-mods/lollipop)
*   [Cucumber Library (by BlakeBr0)](https://www.curseforge.com/minecraft/mc-mods/cucumber)
*   [Architectury API (Forge) (by shedaniel)](https://www.curseforge.com/minecraft/mc-mods/architectury-forge)
*   [FTB Library (Forge) (by FTB)](https://www.curseforge.com/minecraft/mc-mods/ftb-library-forge)
*   [FTB Teams (Forge) (by FTB)](https://www.curseforge.com/minecraft/mc-mods/ftb-teams-forge)
*   [Ignition: EnderBags (by Shadows\_of\_Fire)](https://www.curseforge.com/minecraft/mc-mods/ignition-enderbags)
*   [GraveStone Mod (by henkelmax)](https://www.curseforge.com/minecraft/mc-mods/gravestone-mod)
*   [Waystones (by BlayTheNinth)](https://www.curseforge.com/minecraft/mc-mods/waystones)
*   [Konkrete \[Forge\] (by Keksuccino)](https://www.curseforge.com/minecraft/mc-mods/konkrete)
*   [Botanical Machinery (by MelanX)](https://www.curseforge.com/minecraft/mc-mods/botanical-machinery)
*   [Wawla - What Are We Looking At (by DarkhaxDev)](https://www.curseforge.com/minecraft/mc-mods/wawla)
*   [Silent Lib (silentlib) (by SilentChaos512)](https://www.curseforge.com/minecraft/mc-mods/silent-lib)
*   [MythicBotany (by noeppinoeppi)](https://www.curseforge.com/minecraft/mc-mods/mythicbotany)
*   [LibX (by noeppinoeppi)](https://www.curseforge.com/minecraft/mc-mods/libx)
*   [Traveler's Backpack (by Tiviacz1337)](https://www.curseforge.com/minecraft/mc-mods/travelers-backpack)
*   [Infernal Expansion (by InfernalStudios)](https://www.curseforge.com/minecraft/mc-mods/infernal-expansion)
*   [Chisels & Bits (by AlgorithmX2)](https://www.curseforge.com/minecraft/mc-mods/chisels-bits)
*   [Runelic (by DarkhaxDev)](https://www.curseforge.com/minecraft/mc-mods/runelic)
*   [AIOT Botania (by MelanX)](https://www.curseforge.com/minecraft/mc-mods/aiot-botania)
*   [Titanium (by Buuz135)](https://www.curseforge.com/minecraft/mc-mods/titanium)
*   [SwingThroughGrass (by exidex)](https://www.curseforge.com/minecraft/mc-mods/swingthroughgrass)
*   [Baubles - Reborn? (by lazynessmind)](https://www.curseforge.com/minecraft/mc-mods/baubles-reborn)
*   [ExNihilo: Automation (by lazynessmind)](https://www.curseforge.com/minecraft/mc-mods/exnihilo-automation)
*   [Pam's HarvestCraft 2 - Trees (by pamharvestcraft)](https://www.curseforge.com/minecraft/mc-mods/pams-harvestcraft-2-trees)
*   [Dark Utilities (by DarkhaxDev)](https://www.curseforge.com/minecraft/mc-mods/dark-utilities)
*   [Botania: Garden of Glass (by Vazkii)](https://www.curseforge.com/minecraft/mc-mods/botania-garden-of-glass)
*   [Rope Bridge (by lordcazsius)](https://www.curseforge.com/minecraft/mc-mods/rope-bridge)
*   [Mystical Agriculture (by BlakeBr0)](https://www.curseforge.com/minecraft/mc-mods/mystical-agriculture)
*   [Scaling Health (by SilentChaos512)](https://www.curseforge.com/minecraft/mc-mods/scaling-health)
*   [Tinkers Construct (by mDiyo)](https://www.curseforge.com/minecraft/mc-mods/tinkers-construct)
*   [Crafting Tweaks (by BlayTheNinth)](https://www.curseforge.com/minecraft/mc-mods/crafting-tweaks)
*   [Create (by simibubi)](https://www.curseforge.com/minecraft/mc-mods/create)
*   [Construction Wand (by ThetaDev)](https://www.curseforge.com/minecraft/mc-mods/construction-wand)
*   [FancyMenu \[Forge\] (by Keksuccino)](https://www.curseforge.com/minecraft/mc-mods/fancymenu)
*   [Apotheosis (by Shadows\_of\_Fire)](https://www.curseforge.com/minecraft/mc-mods/apotheosis)
*   [Patchouli (by Vazkii)](https://www.curseforge.com/minecraft/mc-mods/patchouli)
*   [Mantle (by mDiyo)](https://www.curseforge.com/minecraft/mc-mods/mantle)
*   [Building Gadgets (by Direwolf20)](https://www.curseforge.com/minecraft/mc-mods/building-gadgets)
*   [Quantum Quarry Plus (by inzhefop)](https://www.curseforge.com/minecraft/mc-mods/quantum-querry-plus)
*   [JourneyMap (by techbrew)](https://www.curseforge.com/minecraft/mc-mods/journeymap)
*   [Morpheus (by Quetzi)](https://www.curseforge.com/minecraft/mc-mods/morpheus)
*   [Ex Compressum (by BlayTheNinth)](https://www.curseforge.com/minecraft/mc-mods/ex-compressum)
*   [CraftTweaker (by Jaredlll08)](https://www.curseforge.com/minecraft/mc-mods/crafttweaker)
*   [Refined Storage (by raoulvdberge)](https://www.curseforge.com/minecraft/mc-mods/refined-storage)
*   [OpenBlocks Elevator (by vsngarcia)](https://www.curseforge.com/minecraft/mc-mods/openblocks-elevator)
*   [Neat (by Vazkii)](https://www.curseforge.com/minecraft/mc-mods/neat)
*   [Tool Belt (by gigaherz)](https://www.curseforge.com/minecraft/mc-mods/tool-belt)
*   [Farming for Blockheads (by BlayTheNinth)](https://www.curseforge.com/minecraft/mc-mods/farming-for-blockheads)
*   [Time in a bottle standalone (by haoict)](https://www.curseforge.com/minecraft/mc-mods/time-in-a-bottle-standalone)
*   [CobbleForDays (by LexManos)](https://www.curseforge.com/minecraft/mc-mods/cobblefordays)
*   [Botania (by Vazkii)](https://www.curseforge.com/minecraft/mc-mods/botania)
*   [Angel Ring (by DenisMasterHerobrine)](https://www.curseforge.com/minecraft/mc-mods/angel-ring)
*   [CodeChicken Lib 1.8.+ (by covers1624)](https://www.curseforge.com/minecraft/mc-mods/codechicken-lib-1-8)
*   [Skyblock Builder (by MelanX)](https://www.curseforge.com/minecraft/mc-mods/skyblock-builder)
*   [Akashic Tome (by Vazkii)](https://www.curseforge.com/minecraft/mc-mods/akashic-tome)
*   [Danny's Expansion (by bottomtextdanny)](https://www.curseforge.com/minecraft/mc-mods/beta-dannys-expansion)
*   [Polymorph (Forge) (by TheIllusiveC4)](https://www.curseforge.com/minecraft/mc-mods/polymorph)
*   [Default World Type \[Forge\] (by MelanX)](https://www.curseforge.com/minecraft/mc-mods/defaultworldtype)
*   [Pam's HarvestCraft 2 - Crops (by pamharvestcraft)](https://www.curseforge.com/minecraft/mc-mods/pams-harvestcraft-2-crops)
*   [inzhefop's Core (by inzhefop)](https://www.curseforge.com/minecraft/mc-mods/inzhefop-core)
*   [Cooking for Blockheads (by BlayTheNinth)](https://www.curseforge.com/minecraft/mc-mods/cooking-for-blockheads)
*   [Quark (by Vazkii)](https://www.curseforge.com/minecraft/mc-mods/quark)
*   [Just Enough Resources (JER) (by way2muchnoise)](https://www.curseforge.com/minecraft/mc-mods/just-enough-resources-jer)
*   [Botany Pots (by DarkhaxDev)](https://www.curseforge.com/minecraft/mc-mods/botany-pots)
*   [ExtraStorage (by 3divad99)](https://www.curseforge.com/minecraft/mc-mods/extrastorage)
*   [Ceramics (by KnightMiner)](https://www.curseforge.com/minecraft/mc-mods/ceramics)
*   [Collective (by Serilum)](https://www.curseforge.com/minecraft/mc-mods/collective)
*   [Macaw's Bridges (by sketch\_macaw)](https://www.curseforge.com/minecraft/mc-mods/macaws-bridges)
*   [Ore Excavation (by Funwayguy)](https://www.curseforge.com/minecraft/mc-mods/ore-excavation)
*   [Clumps (by Jaredlll08)](https://www.curseforge.com/minecraft/mc-mods/clumps)
*   [Ultimate Skyblock Resource Generator (by rafacost3D)](https://www.curseforge.com/minecraft/mc-mods/ultimate-skyblock-generator)
*   [Nature's Compass (by Chaosyr)](https://www.curseforge.com/minecraft/mc-mods/natures-compass)
*   [FTB Quests (Forge) (by FTB)](https://www.curseforge.com/minecraft/mc-mods/ftb-quests-forge)
*   [Dank Storage (by tfarecnim)](https://www.curseforge.com/minecraft/mc-mods/dank-storage)
*   [Comforts (Forge) (by TheIllusiveC4)](https://www.curseforge.com/minecraft/mc-mods/comforts)
*   [Iron Chests (by ProgWML6)](https://www.curseforge.com/minecraft/mc-mods/iron-chests)
*   [AutoRegLib (by Vazkii)](https://www.curseforge.com/minecraft/mc-mods/autoreglib)
*   [Mob Grinding Utils (by vadis365)](https://www.curseforge.com/minecraft/mc-mods/mob-grinding-utils)
*   [FTB Ranks (Forge) (by FTB)](https://www.curseforge.com/minecraft/mc-mods/ftb-ranks-forge)
*   [FTB Chunks (Forge) (by FTB)](https://www.curseforge.com/minecraft/mc-mods/ftb-chunks-forge)
*   [Configurable Extra Mob Drops (by Serilum)](https://www.curseforge.com/minecraft/mc-mods/configurable-extra-mob-drops)
*   [Cyclic (by Lothrazar)](https://www.curseforge.com/minecraft/mc-mods/cyclic)
*   [Curios API (Forge) (by TheIllusiveC4)](https://www.curseforge.com/minecraft/mc-mods/curios)
*   [Pam's HarvestCraft 2 - Food Core (by pamharvestcraft)](https://www.curseforge.com/minecraft/mc-mods/pams-harvestcraft-2-food-core)
*   [Materials Expanded (by michaelsixtyfour)](https://www.curseforge.com/minecraft/mc-mods/matex)
*   [Mod Name Tooltip (by mezz)](https://www.curseforge.com/minecraft/mc-mods/mod-name-tooltip)
*   [Random Loot Mod (by milomaz1)](https://www.curseforge.com/minecraft/mc-mods/random-loot-mod)
*   [Inventory Tweaks Renewed (by David1544)](https://www.curseforge.com/minecraft/mc-mods/inventory-tweaks-renewed)
*   [Mouse Tweaks (by YaLTeR)](https://www.curseforge.com/minecraft/mc-mods/mouse-tweaks)
*   [Torchmaster (by xalcon)](https://www.curseforge.com/minecraft/mc-mods/torchmaster)
*   [Item Filters (Forge) (by LatvianModder)](https://www.curseforge.com/minecraft/mc-mods/item-filters-forge)
*   [Controlling (by Jaredlll08)](https://www.curseforge.com/minecraft/mc-mods/controlling)
*   [Hwyla (by TehNut)](https://www.curseforge.com/minecraft/mc-mods/hwyla)
*   [Placebo (by Shadows\_of\_Fire)](https://www.curseforge.com/minecraft/mc-mods/placebo)
*   [JEI Integration (by SnowShock35)](https://www.curseforge.com/minecraft/mc-mods/jei-integration)
