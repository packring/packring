async function reloadPacks() {
	var packs = (await ajaxRequest("/packring/packs.txt", true, "text/plain"))
		.split("\n")
		.sort()
		.filter(element =>
		{
			return element !== '' && element !== undefined && typeof element !== 'undefined';
		});

	var categories = location.getParams()["categories"];
	if (!isUndefined(categories)) {
		categories = JSON.parse(decodeURIComponent(categories));
	}
	else {
		var similarPack = location.getParams()["similarto"];
		if (!isUndefined(similarPack)) {
			similarPack = await ajaxRequest("/packring/packs/"+decodeURIComponent(similarPack)+".md");
			var similarPackYAML = jsyaml.load(
				/^---[\r\n]*(.*)^---/gsm.exec(
					similarPack
				)[1]
			)
		}
	}

	document.getElementById("packlist").innerHTML = "";
	var numberOfPacks = 0;
	for (i = 0; i < packs.length; i++) {
		let content = await ajaxRequest("/packring/packs/"+packs[i]);
		if (typeof content === 'string') {
			let yaml = jsyaml.load(
				/^---[\r\n]*(.*)^---/gsm.exec(
					content
				)[1]
			);
			if (!isUndefined(categories) &&
				!categories.every(v => yaml.Categories.includes(v))) {//some = inclusive, every = exclusive
				continue;
			}

			if (!isUndefined(similarPackYAML)) {
				document.title = "Affiliated Modpacks similar to "+similarPackYAML.Title;
				let similarityRatio = (yaml.Categories.filter(function (elem) {
					return similarPackYAML.Categories.indexOf(elem) > -1;
					})
					.length / yaml.Categories.length
				);
				if (similarityRatio < 0.5 || similarPackYAML.URL == yaml.URL) {
					continue;
				}
			}

			let div = insertDOMElement("div", "", document.getElementById("packlist"));
			div.setAttribute("class", "packicon");

			let link = insertDOMElement("a", "", div);
			link.setAttribute("href", "modpack?"+packs[i].replace(/\.[^/.]+$/, ""));
			link.setAttribute("class", "nodeco");

			let covertext = insertDOMElement("div", yaml.Title, link);
			covertext.setAttribute("class", "covertext");
			
			let img = insertDOMElement("img", "", link);
			img.setAttribute("class", "packimage");
			img.setAttribute("src", "res/"+yaml.Image);
			img.setAttribute("alt", yaml.Title);
			img.setAttribute("title", yaml.Title);

			if (yaml.Description.length > 65) {
				yaml.Description = yaml.Description.substring(0, 64)+"&#8230;";
			}
			insertDOMElement("p", yaml.Description, link);

			numberOfPacks++;
		}
		else {
			console.log("error "+content);
		}
	}

	if (numberOfPacks === 0) {
		let div = insertDOMElement("div", "No entries found.", document.getElementById("packlist"));
		div.setAttribute("class", "packlist-text");
	}
}

function menuCheckedChanged() {
	let checkboxes = document.getElementsByName("menu-option");
	let checkboxesChecked = [];
	for (let i=0; i<checkboxes.length; i++) {
		if (checkboxes[i].checked) {
			checkboxesChecked.push(checkboxes[i].value);
		}
	}
	if (checkboxesChecked.length > 0) {
		location.alterSearchValue("?categories=" + encodeURIComponent(JSON.stringify(checkboxesChecked)));
	}
	else {
		location.alterSearchValue();
	}
	reloadPacks();
}

window.onclick = function(event) {
	if (!event.target.matches('#menu-button, #menu-button *, #menu-content, #menu-content *')) {
		let dropdown = document.getElementById("menu-content");
		if (dropdown.classList.contains('show-menu')) {
			dropdown.classList.remove('show-menu');
		}
	}
}

document.addEventListener("DOMContentLoaded", async function() {
	var allCategories = (await ajaxRequest("/packring/categories.txt", true, "text/plain")).split("\n");
	let menuContent = document.getElementById("menu-content");
	for (let i = 0; i < allCategories.length; i++) {
		let p = insertDOMElement("p", "", menuContent);
		p.setAttribute("class", "menu-option-casing");

		let label = insertDOMElement("label", "", p);

		let checkbox = insertDOMElement("input", "", label);
		checkbox.setAttribute("type", "checkbox");
		checkbox.setAttribute("name", "menu-option");
		checkbox.setAttribute("class", "menu-option");
		checkbox.setAttribute("value", allCategories[i]);
		checkbox.onclick = menuCheckedChanged;

		insertDOMElement("_text_",
			allCategories[i]
				.replace(/\b\w/g, (c) => c.toUpperCase()),
			label);
	}

	document.getElementById("menu-button").onclick = function() {
		document.getElementById("menu-content").classList.toggle("show-menu");
	}

	reloadPacks();
}, false);
