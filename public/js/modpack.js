var converter = new showdown.Converter();
document.addEventListener("DOMContentLoaded", async function() {
	let contentElem = document.getElementById("content");
	let text = await ajaxRequest("/packring/packs/"+location.getParamsText()+".md");
	let yaml = jsyaml.load(
		/^---[\r\n]*(.*)^---/gsm.exec(
			text
		)[1]
	);
	let markdown = converter.makeHtml(
		/\n---(.*(?!---).*)$/gs.exec(
			text
		)[1]
	);
	
	if (!isUndefined(yaml.Color)) {
		insertDOMElement("style", "button{--color: "+yaml.Color+"}", document.head);
	}
	
	document.title = yaml.Title;
	document.head.children.namedItem('description').content = yaml.Description;
	
	let similarHyper = insertDOMElement("a", "", document.getElementById("header"));
	similarHyper.setAttribute("href", "/packring/?similarto="+location.getParamsText());
	insertDOMElement("button", "More Like This", similarHyper)
		.setAttribute("style", "float: right;");
	
	let packHeader = document.getElementById("packheader");
	
	insertDOMElement("h1", yaml.Title, packHeader).
		setAttribute("style", "display: inline-block; padding: 4px 0;");
	
	let downloadHyper = insertDOMElement("a", "", packHeader);
	downloadHyper.setAttribute("href", yaml.URL);
	downloadHyper.setAttribute("style", "float: right;");
	
	let downloadButton = insertDOMElement("button", "", downloadHyper);
	if (window.matchMedia("(orientation: portrait)").matches) {
		downloadButton.setAttribute("class", "icon");
		downloadButton.setAttribute("title", "Download Pack");
		
		let iconImg = insertDOMElement("img", "", downloadButton);
		iconImg.setAttribute("src", "/packring/res/download.svg");
		iconImg.setAttribute("class", "icon");
	}
	else {
		downloadButton.innerHTML = "Download";
	}
	
	let div = insertDOMElement("div", markdown, contentElem);
	div.setAttribute("id", "packdescription");
	
	insertDOMElement("h1", "&nbsp;", contentElem);
}, false);