# PackRing
![PackRing](https://gitlab.com/packring/packring/-/raw/main/resources/packring-logo.png)

PackRing is a simple, frontend-only webring-like, designed for Minecraft modpacks.



Feel free to fork this for your own ring! Works via GitLab/GitHub pages, so no additional hosting is required!

Fair warning that if your fork isn't called `packring `, some of your links will be broken unless you change the associated paths to include your new fork name instead (for example, if your fork was called `my-modpacks`, you'd need to change lines like `<script src="/packring/js/ajax.js"></script>` and `await ajaxRequest("/packring/packs/"+packs[i])` to read `<script src="/my-modpacks/js/ajax.js"></script>` and `await ajaxRequest("/my-modpacks/packs/"+packs[i])` respectively).

## Adding Pages

Pages are added by including them as a MarkDown file in `/packs/`, with any uploaded resources they require stored in `/res/`, and then adding the MarkDown file's name to `/packs.txt`. This can be done via pull request, in the case that you'd like people to submit entries to your ring.

If you are submitting an entry to *this* ring, you are required to have a link to the webring ([https://packring.gitlab.io/packring/](https://packring.gitlab.io/packring/)) on your modpack's page (usually its CurseForge page), and if possible, its main menu. In addition, you need to join the ring's discord ([https://discord.gg/N8m7abMCsV](https://discord.gg/N8m7abMCsV)). Make sure you have these done *before* submitting an entry, or it may be denied.

The pack MarkDown files must have additional metadata stored as YAML data at the beginning of the file as below:

```
---
Title: 'This text will be used for the title (hover) text of the entry''s icon, as well as the header and page title for the entry''s info page'
Author: 'This is not currently displayed anywhere on the page, but it''s good information to track'
Description: 'This text is displayed below the entry''s icon, and is listed in the entry''s info page''s metadata'
Image: 'This is the filename (relative to the /res/ directory) of the icon to be displayed for the entry on the index page'
URL: 'This is the URL to which the info page''s header will link.'
Categories: ['This entry is a list of', 'categories under which your pack falls.', 'At the moment, they are roughly synonymous with CurseForge''s modpack categories.']
Color: (optional) defines the hue of buttons on the entry's page.
---
```

The categories currently available are as follows:

- `tech`
- `magic`
- `scifi`
- `adventure`
- `exploration`
- `minigame`
- `quests`
- `hardcore`
- `map-based`
- `light`
- `massive`
- `combat`
- `multiplayer`
- `skyblock`



This project makes use of [JS-YAML](https://github.com/nodeca/js-yaml) and [Showdown](https://github.com/showdownjs/showdown). All other code is original.

In addition, the project also contains and makes use of the fonts Montserrat and Open Sans.

Finally, the project contains a modified version of the webring icon from [XXIIVV](https://github.com/XXIIVV/webring), as well as several icons from [Feather](https://github.com/feathericons/feather). All other images are either original or the property and responsibility of the owners of the pack pages/icons in which they are exclusively used.
